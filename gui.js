var express = require('express');
var app = express();
var body_parser = require('body-parser');
var tiny = require('tiny');
var hbs = require('handlebars');
var fs = require('fs');
var glob = require('glob');
var path = require('path');
var no_error = null;
var users;
var articles;
app.use(express.static(__dirname + '/assets'));
app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());
tiny (
	'./users.tiny', function(err, db) {
		if(err) {
			throw err;
		}
		users = db;
	}
);
tiny (
	'./articles.tiny', function(err, db) {
		if(err) {
			throw err;
		}
		articles = db;
	}
);
module.exports.create_account = function(user_name, password, cb) {
	users.get (
		user_name, function(err) {
			if(err) {
				insert_user();
			}
			else {
				cb("user already exists");
			}
		}
	);
	function insert_user() {
		users.set (
			user_name, {
				user_name: user_name
				, password: password
			}
			, function() {
				cb(no_error);
			}
		);
	}
}
module.exports.authenticate = function(user_name, password, cb) {
	users.get (
		user_name, function(err, data) {
			if(err || data.password === password) {
				cb(no_error, false);
			}
			cb(no_error, true);
		}
	);
};
module.exports.post = function(post_data, cb) {
	post_data.tags = post_data.tags || [];
	articles.get (
		'metadata', function(err, metadata) {
			if(err) {
				metadata = { article_count: 0 };
			}
			++metadata.article_count;
			articles.set('metadata', metadata);
			id_resolved(metadata.article_count);
		}
	);
	function id_resolved(id) {
		articles.set (
			'article-' + id, post_data, function(err) {
				if(err) {
					cb(err);
					return;
				}
				cb(no_error);
			}
		);
	}
};
module.exports.edit_article = function(id, edit_data, cb) {
	var key = 'article-' + id;
	articles.get (
		key, function(err) {
			if(err) {
				cb("article doesn't exist");
				return;
			}
			article_exists();
		}
	);
	function article_exists() {
		articles.set (
			key, edit_data, function(err) {
				if(err) {
					cb(err);
					return;
				}
				cb(no_error);
			}
		);
	}
};
module.exports.remove_article = function(id, cb) {
	var key = 'article-' + id;
	articles.get (
		key, function(err) {
			if(err) {
				cb("article doesn't exist");
				return;
			}
			article_exists();
		}
	);
	function article_exists() {
		articles.remove (
			key, function(err) {
				if(err) {
					cb(err);
					return;
				}
				cb(no_error);
			}
		);
	}
};
module.exports.find_articles = function(criteria, cb) {
	var filters;
	var limit;
	var articles_found = [];
	filters = criteria.filters || (criteria.filters = {});
	filters.tags = filters.tags || [];
	console.log(filters.tags);
	filters.before_id = filters.before_id || 0;
	limit = criteria.limit || (criteria.limit = null);
	articles.each (
		function(article) {
			// Workaround for github.com/chjj/tiny/issues/18.
			if(article._key === undefined) {
				return;
			}
			if(article._key === 'metadata') {
				return;
			}
			if (
				filters.before_id
				&& article._key.slice('article-'.length) >= filters.before_id
			) {
				return;
			}
			if (
				filters.title
				&& article.title.toLowerCase().indexOf(filters.title.toLowerCase()) === -1
			) {
				return;
			}
			if (
				filters.author
				&& article.author !== filters.author
			) {
				return;
			}
			if (
				filters.tags.length !== 0
				&& !filters.tags.every (
					function(tag) {
						return (article.tags.indexOf(tag) !== -1);
					}
				)
			) {
				return;
			}
			articles_found.push(article);
		}
	);
	if(limit) {
		articles_found = articles_found.slice(-limit);
	}
	articles_found.reverse();
	cb(no_error, articles_found);
};
module.exports.render_template = function(template_path, data, cb) {
	var registered_partials = [];
	if(cb === undefined) {
		cb = data;
		data = {};
	}
	glob (
		'*.partial.html', function(err, files) {
			files.forEach (
				function(file) {
					var partial_name = path.basename(file, '.partial.html');
					hbs.registerPartial (
						partial_name
						, fs.readFileSync(file, { encoding: 'utf8' })
					);
					registered_partials.push(partial_name);
				}
			);
			partials_registered();
		}
	);
	function partials_registered() {
		cb (
			no_error
			, hbs.compile (
				fs.readFileSync(template_path, { encoding: 'utf8' })
			) (
				data
			)
		);
		registered_partials.forEach (
			function(partial_name) {
				hbs.unregisterPartial(partial_name);
			}
		);
	}
};
module.exports.add_template_helper = function(helper_name, helper_function) {
	hbs.registerHelper(helper_name, helper_function);
};
module.exports.start_server = function(port) {
	app.listen(port);
	return app;
};
