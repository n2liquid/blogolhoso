/*
	Funções da biblioteca gui:

	* server = gui.start_server(port)
		* server.get(endpoint, cb(request, response))
		* server.post(endpoint, cb(request, response))
	* gui.render_template(template_path, ?data, cb(err, html))
	* gui.create_account(user, password, cb(err))
	* gui.authenticate(user, password, cb(err))
	* gui.post({ title, author, ?tags, body }, cb(err))
	* gui.edit_article(id, { title, author, tags, body }, cb(err))
	* gui.find_articles(?{ ?filters: { ?title, ?author, ?from_id }, ?limit }, cb(err, [ { title, author, tags, body } ])
	* gui.add_template_helper(helper_name, helper_function)
*/
var gui = require('./gui');
var server = gui.start_server(8000);
server.get (
	'/', function(request, response) {
		gui.render_template (
			'index.html', function(err, html) {
				response.send(html);
			}
		);
	}
);
